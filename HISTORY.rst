=======
History
=======

0.3.0 (2022-09-22)
------------------

* Ajout du module `repr`.

0.1.0 (2021-11-25)
------------------

* First release on PyPI.
