Documentation du paquet Python lyceum
======================================

.. toctree::
   :maxdepth: 2
   :caption: Table des matières:
   :glob:

   readme
   installation
   examples/*
   modules
   contributing
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
